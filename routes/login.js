const express = require("express");

//const bcrypt = require("bcrypt");

import bcrypt from "bcrypt";

var jwt = require('jsonwebtoken');

const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(process.env.CLIENT_ID)

import Usuario from "../models/usuario";

const app = express();

app.post("/login", async(req, res) => {

    const {email, password} = req.body;

    console.log("Este es solo email ", email);
    console.log("Este es solo password ", password + ' mis huveos') ;

try{
    const usuarioDB = await Usuario.findOne({ email: email});
    console.log("Este es solo usuarioDB ", usuarioDB);
        if(!usuarioDB){
            return res.status(401).json({
                ok: false,
                err:{
                    message: "(Usuario) o contraseña incorrectas",
                },
            });
        }
        console.log(bcrypt.compareSync(password, usuarioDB.password))
        console.log(password.length)
        if(!bcrypt.compareSync(password, usuarioDB.password)) {
            return res.status(402).json({
                ok: false,
                err: {
                    message: "Usuario o (contraseña) incorrectas",
                },
            });
        }
        const token = jwt.sign({
            usuario: usuarioDB
        }, process.env.SEED, {expiresIn: process.env.CADUCIDAD_TOKEN})
        return res.json({
            ok: true,
            usuario: usuarioDB,
            token: token,
        })
    }catch(err){
        console.log("Error 500: " + err)
        res.status(500).json({
            ok: false,
            message: "Error catch",
            err,
        })
    }

});

async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID,
    });
    const payload = ticket.getPayload();

    console.log(payload.name);
    console.log(payload.email);
    console.log(payload.picture);

    return payload;
}

app.post("/google", async (req, res) => {
    let token = req.body.idtoken;

    console.log("Este es solo token ", token);

     let googleUser = await verify(token).catch((e) => {
        return res.status(403).json({
            ok: false,
            err: e,
            mesaje: 'ERROR CON EL TOKEN'
        });
    }); 

    Usuario.findOne({ email: googleUser.email }, (err, usuarioDB) => {
        if(err){
            return res.status(500).status.json({
                ok: false,
                err,
            });
        }
        if (usuarioDB) {
            if (usuarioDB.google === false) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: "Debe de usar su autentificacion normal",
                    },
                });
            } else {
                let token = jwt.sign(
                    {
                        usuario: usuarioDB,
                    },
                    process.env.SEED,
                    { expiresIn: process.env.CADUCIDAD_TOKEN }
                );

                return res.json({
                    ok: true,
                    usuario: usuarioDB,
                    token,
                });
            }
        } else {
            let usuario = new Usuario();

            usuario.nombre = googleUser.name;
            usuario.email = googleUser.email;
            usuario.img = googleUser.picture;
            usuario.google = true;
            usuario.password = "123";

            usuario.save((err, usuarioDB) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        err,
                    });
                }

                let token = jwt.sign(
                    {
                        usuario: usuarioDB,
                    },
                    process.env.SEED,
                    { expiresIn: process.env.CADUCIDAD_TOKEN }
                );

                return res.json({
                    ok: true,
                    usuario: usuarioDB,
                    token,
                });
            });
        }
    });
    //verify(token);
});

module.exports = app;